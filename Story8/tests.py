from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
import time

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)


    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()

    def test_dropdown_works(self):
        self.browser.get('http://127.0.0.1:8000/Story8')
        time.sleep(5)

        # CHECK IF TITLE IS DISPLAYED RIGHT
        header = self.browser.find_element_by_xpath('/html/body/div[1]').text
        self.assertEquals("ACCORDION", header)

        # WHEN CLICKED, CHECK PANEL'S VISIBILITY AND OTHER PANEL'S INVISIBILITY
        self.browser.find_element_by_xpath('/html/body/div[2]/div[1]/div[1]/button[1]').click()
        panel1 = self.browser.find_element_by_xpath('/html/body/div[2]/div[1]/div[2]')
        panel2 = self.browser.find_element_by_xpath('/html/body/div[2]/div[2]/div[2]')

        self.assertTrue(panel1.is_displayed())
        self.assertFalse(panel2.is_displayed())

        # CHECK IF UP BUTTON WORKS
        self.browser.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/button[3]').click()
        head1 = self.browser.find_element_by_xpath('/html/body/div[2]/div[1]/div[1]/button[1]').text
        head2 = self.browser.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/button[1]').text
        self.assertEquals('PENGALAMAN ORGANISASI/KEPANITIAAN', head1)
        self.assertEquals('AKTIVITAS SAAT INI', head2)
        time.sleep(3)

        # CHECK IF DOWN BUTTON WORKS
        self.browser.find_element_by_xpath('/html/body/div[2]/div[1]/div[1]/button[2]').click()
        head1 = self.browser.find_element_by_xpath('/html/body/div[2]/div[1]/div[1]/button[1]').text
        head2 = self.browser.find_element_by_xpath('/html/body/div[2]/div[2]/div[1]/button[1]').text
        self.assertEquals('AKTIVITAS SAAT INI', head1)
        self.assertEquals('PENGALAMAN ORGANISASI/KEPANITIAAN', head2)
        time.sleep(3)
