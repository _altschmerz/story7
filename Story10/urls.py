from django.urls import path
from .views import register, loginPage, logoutUser, random
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('login/', loginPage, name="loginPage"),
    path('logout/', logoutUser,name="logoutUser"),
    path('register/', register, name="register"),
    path('random/', random, name="random")
]