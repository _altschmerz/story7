from django.shortcuts import render, redirect
from .forms import PersonForm
from .models import Person

# Create your views here.
def index(request):
    if (request.method == "POST"):
        form = PersonForm(request.POST)
        if form.is_valid():
            request.session['input'] = request.POST
            request.session['name'] = request.POST['name']
            request.session['status'] = request.POST['status']
            return redirect('confirmation')

    form = PersonForm()
    data = Person.objects.all()
    return render(request, 'index.html', {'form' : form, 'data' : data})

def confirmation(request):
    if (request.method == "POST"):
        if '_yes' in request.POST:
            form = PersonForm(request.session.get('input'))
            form.save()
            return redirect('/')
        elif '_no' in request.POST:
            return redirect('/')

    name = request.session.get('name')
    status = request.session.get('status')
    return render(request, 'confirmation.html', {'name' : name, 'status' : status})