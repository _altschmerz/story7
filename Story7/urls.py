from django.urls import path
from .views import index, confirmation

urlpatterns = [
    path('', index, name='index'),
    path('confirmation', confirmation, name='confirmation')
]