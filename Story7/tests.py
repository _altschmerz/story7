from django.test import TestCase, Client
from .models import Person
from .forms import PersonForm
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.options import Options
import time

class UnitTest(TestCase):
    def test_index_url_exists(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

  
    def test_index_using_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')


    def test_model_is_working(self):
        person = Person.objects.create()
        count = Person.objects.all().count()
        self.assertEqual(count, 1)


    def test_model_has_the_right_data(self):
        person = Person.objects.create(name='Erica', status='Hi there!')
        name = person.name
        status = person.status
        self.assertEqual(name, 'Erica')
        self.assertEqual(status, 'Hi there!')


    def test_form_validity(self):
        form_data = {'name' : 'Erica', 'status' : 'Hi there!'}
        form = PersonForm(data=form_data)
        self.assertTrue(form.is_valid())


    def test_from_invalidity(self):
        form_data = {'name' : '', 'status' : ''}
        form = PersonForm(data=form_data)
        self.assertFalse(form.is_valid())



class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)


    def tearDown(self):
        self.browser.get('http://127.0.0.1:8000/admin')
        time.sleep(5)

        self.browser.find_element_by_xpath('//*[@id="id_username"]').send_keys('admin')
        self.browser.find_element_by_xpath('//*[@id="id_password"]').send_keys('admin')
        self.browser.find_element_by_xpath('//*[@id="login-form"]/div[3]/input').click()
        time.sleep(5)   

        self.browser.find_element_by_xpath('//*[@id="content-main"]/div[2]/table/tbody/tr/th/a').click()
        self.browser.find_element_by_xpath('//*[@id="action-toggle"]').click()
        self.browser.find_element_by_xpath('//*[@id="changelist-form"]/div[1]/label/select/option[2]').click()
        self.browser.find_element_by_xpath('//*[@id="changelist-form"]/div[1]/button').click()
        self.browser.find_element_by_xpath('//*[@id="content"]/form/div/input[4]').click()

        self.browser.implicitly_wait(3)
        self.browser.quit()


    def test_status_posting(self):
        self.browser.get("http://127.0.0.1:8000")
        time.sleep(5)

        header = self.browser.find_element_by_xpath('/html/body/div[1]').text
        self.assertIn("STATUS", header)

        # CHECK STATUS POSTED WHEN CONFIRMED
        self.browser.find_element_by_xpath('//*[@id="id_name"]').send_keys('Dummy1')
        self.browser.find_element_by_xpath('//*[@id="id_status"]').send_keys('Hi there!')
        self.browser.find_element_by_xpath('/html/body/form/button').click()

        self.browser.find_element_by_xpath('/html/body/form/input[2]').click()

        name_display = self.browser.find_element_by_xpath('/html/body/div/div/div[1]').text
        stat_display = self.browser.find_element_by_xpath('/html/body/div/div/div[2]').text
        self.assertIn('Dummy1', name_display)
        self.assertIn('Hi there!', stat_display)

        # CHECK STATUS NOT POSTED WHEN NOT CONFIRMED
        self.browser.find_element_by_xpath('//*[@id="id_name"]').send_keys('Dummy2')
        self.browser.find_element_by_xpath('//*[@id="id_status"]').send_keys('Hello there!')
        self.browser.find_element_by_xpath('/html/body/form/button').click()

        self.browser.find_element_by_xpath('/html/body/form/input[3]').click()

        stats = self.browser.find_element_by_xpath('/html/body/div').text
        self.assertNotIn('Dummy2', stats)
        self.assertNotIn('Hello there!', stats)