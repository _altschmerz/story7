from django.shortcuts import render
from .models import Book
from .serializers import BookSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response

def booklist(request):
    return render(request, 'booklist.html')

@api_view(['POST'])
def add_liked_book(request):
    serializer = BookSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['GET'])
def get_top_5_books(request):
    books = Book.objects.all().order_by("-likes")[:5]
    serializer = BookSerializer(books, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def add_like(request, pk):
    book = Book.objects.get(id=pk)
    book.likes += 1
    book.save()
    print(book.likes)
    serializer = BookSerializer(instance=book, data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)
