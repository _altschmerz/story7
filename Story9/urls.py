from django.urls import path
from .views import booklist, add_liked_book, get_top_5_books, add_like

urlpatterns = [
    path('', booklist, name='booklist'),
    path('api/add_liked_book/', add_liked_book, name='add_liked_book'),
    path('api/get_top_5_books/', get_top_5_books, name='get_top_5_books'),
    path('api/add_like/<str:pk>', add_like, name='add_like')
]