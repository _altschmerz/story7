from django.db import models

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=256)
    img_link = models.CharField(max_length=2048)
    likes = models.IntegerField(default=1)

    def __str__(self):
        return self.title